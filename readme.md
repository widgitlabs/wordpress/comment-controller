# Comment Controller

## Welcome to our GitLab Repository

Comment Controller allows users to disable display of comments for their account,
and allows site admins to selectively disable comments per-post-type or per-role.

### Installation

1. You can clone the GitLab repository: `https://gitlab.com/widgitlabs/wordpress/comment-controller.git`
2. Or download it directly as a ZIP file: `https://gitlab.com/widgitlabs/wordpress/comment-controller/-/archive/master/comment-controller-master.zip`

This will download the latest developer copy of Comment Controller.

### Bugs

If you find an issue, let us know [here](https://gitlab.com/widgitlabs/wordpress/comment-controller/issues?state=open)!

### Contributions

Anyone is welcome to contribute to Comment Controller. Please read the
[guidelines for contributing](https://gitlab.com/widgitlabs/wordpress/comment-controller/-/blob/master/CONTRIBUTING.md)
to this repository.
