<?php
/**
 * Core unit test
 *
 * @package     CommentController\Tests\Core
 * @since       1.1.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Core unit tests
 *
 * @since       1.1.0
 */
class Tests_Comment_Controller extends WP_UnitTestCase {


	/**
	 * Test suite object
	 *
	 * @access      protected
	 * @since       1.1.0
	 * @var         object $object The test suite object
	 */
	protected $object;


	/**
	 * Set up this test suite
	 *
	 * @access      public
	 * @since       1.1.0
	 * @return      void
	 */
	public function setUp() {
		parent::setUp();
		$this->object = Comment_Controller();
	}


	/**
	 * Tear down this test suite
	 *
	 * @access      public
	 * @since       1.1.0
	 * @return      void
	 */
	public function tearDown() { // phpcs:ignore Generic.CodeAnalysis.UselessOverridingMethod
		parent::tearDown();
	}


	/**
	 * Test the Comment_Controller instance
	 *
	 * @access      public
	 * @since       1.1.0
	 * @return      void
	 */
	public function test_comment_controller_instance() {
		$this->assertClassHasStaticAttribute( 'instance', 'Comment_Controller' );
	}


	/**
	 * Test constants
	 *
	 * @access      public
	 * @since       1.1.0
	 * @return      void
	 */
	public function test_constants() {
		// Make SURE I updated the version.
		$this->assertSame( COMMENT_CONTROLLER_VER, '1.1.4' );

		// Plugin folder URL.
		$path = str_replace( 'tests/', '', plugin_dir_url( __FILE__ ) );
		$this->assertSame( COMMENT_CONTROLLER_URL, $path );

		// Plugin folder path.
		$path               = str_replace( 'tests/', '', plugin_dir_path( __FILE__ ) );
		$path               = substr( $path, 0, -1 );
		$comment_controller = substr( COMMENT_CONTROLLER_DIR, 0, -1 );
		$this->assertSame( $comment_controller, $path );
	}


	/**
	 * Test includes
	 *
	 * @access      public
	 * @since       1.1.0
	 * @return      void
	 */
	public function test_includes() {
		$this->assertFileExists( COMMENT_CONTROLLER_DIR . 'class-comment-controller.php' );
		$this->assertFileExists( COMMENT_CONTROLLER_DIR . 'includes/admin/settings/register-settings.php' );

		$this->assertFileExists( COMMENT_CONTROLLER_DIR . 'includes/misc-functions.php' );

		/** Check Assets Exist */
		$this->assertFileExists( COMMENT_CONTROLLER_DIR . 'assets/banner-772x250.jpg' );
		$this->assertFileExists( COMMENT_CONTROLLER_DIR . 'assets/banner-1544x500.jpg' );
		$this->assertFileExists( COMMENT_CONTROLLER_DIR . 'assets/icon.svg' );
		$this->assertFileExists( COMMENT_CONTROLLER_DIR . 'assets/icon-128x128.jpg' );
		$this->assertFileExists( COMMENT_CONTROLLER_DIR . 'assets/icon-256x256.jpg' );
	}
}
